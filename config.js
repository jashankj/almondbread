
var osd = document.createElement("div");
osd.id = "almondbread";
osd.style.width = "100%";
osd.style.height = "100%";
var body = document.body;
if (body == null)
	body = document.body = document.createElement("body");
body.style.backgroundColor = "black";

body.append(osd);

/**
 * the image is "1024px" wide.
 * l=10 => 2x2 => (-1,-1)
 * l=11 => 4x4

n = 2^(zoom-9)

coords of boxes: (0,0) .. (n-1,n-1)
each box is (n-1)/(n+1) tall/wide

 */
function translate_rcl_to_xyz(r, c, l) {
	var n = Math.pow(2, (l - 9));
	var z = l - 2;

	var tile_width = 4 / n;
	var tile_tl_x = -2 + (r * tile_width);
	var x = tile_tl_x + (tile_width / 2)

	var tile_height = 4 / n;
	var tile_tl_y = -2 + (c * tile_height);
	var y = tile_tl_y + (tile_height / 2)

	return [x, -y, z];
}

OpenSeadragon({
	id: "almondbread",
	prefixUrl: "https://cdnjs.cloudflare.com/ajax/libs/openseadragon/2.2.1/images/",
	navigatorSizeRatio: 0.25,
	tileSources: {
		height: 512 * (1 << 29),
		width: 512 * (1 << 29),
		tileSize: 512,
		minLevel: 10,
		tileOverlap: 0,
		getTileUrl: function (l, r, c) {
			var [x, y, z] = translate_rcl_to_xyz (r, c, l);
			//console.log ("{r="+r+",c="+c+",l="+l+"} => {x="+x+",y="+y+",z="+z+"}");
			return "/mandelbrot/2/" + z + "/" + x + "/" + y + "/tile.bmp";
			//return "/julia/-0.4/0.6/" + z + "/" + x + "/" + y + "/tile.bmp";
			//return "/burning-ship/" + z + "/" + x + "/" + y + "/tile.bmp";
		}
	}
});
