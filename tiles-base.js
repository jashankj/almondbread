// Load and configure leaflet.js

window.addEventListener("load", function (e) {
    // Add the CSS
    var leafletLink = document.createElement("link");
    leafletLink.rel = "stylesheet";
    leafletLink.type = "text/css";
    leafletLink.href = "https://unpkg.com/leaflet@1.2.0/dist/leaflet.css";
    
    // Add the map div
    var leaflet = document.createElement("div");
    leaflet.id = "almondbread";
    leaflet.style.width = "100%";
    leaflet.style.height = "100%";
    leaflet.style.display = "block";
    
    if (document.body == null) {
        console.log(document.body);
        document.body = document.createElement("body");
        console.log(document.body);
    }
    document.body.style.backgroundColor = "black";
    document.body.appendChild(leafletLink);
    document.body.appendChild(leaflet);
    
    window.map = L.map('almondbread', {
        zoomSnap: 0,
        center: [0, -0.5],
        zoom: 8,
        zoomAnimationThreshold: 24,
        wheelPxPerZoomLevel: 120,
        zoomSnap: 1
    });

    document.querySelector(".leaflet-container").style.backgroundColor = "black";
    
    window.layer = L.tileLayer('/mandelbrot/2/{z}/{x}/{y}/tile.bmp', {
        minZoom: 6,
        maxZoom: 256,
        maxNativeZoom: 256,
        updateWhenZooming: false,
        keepBuffer: 8,
        tileSize: 512,
        updateInterval: 500
    })
    
    window.layer.getTileUrl = function (coords) {
        var numTiles = Math.pow(2, coords.z - 1);
        var realSize = Math.pow(2, -(coords.z - 9));
        var offset = ((numTiles - 1) / 2)
        var x = (coords.x - offset) * realSize;
        var y = -((coords.y - offset) * realSize);
    
        return '/mandelbrot/2/' + coords.z + '/' + x + '/' + y + '/tile.bmp';
    }

    L.Control.Location = L.Control.extend({
        onAdd: function(map) {
            var loc = L.DomUtil.create('loc');

            // Add the location div
            var loc = document.createElement("div");
            loc.style.color = "white";
            loc.style.fontFamily = "monospace";
            loc.style.backgroundColor = "rgba(0, 0, 0, 0.6)";
            loc.style.padding = "8px";
            loc.style.borderRadius = "4px";
            loc.style.fontSize = "12pt";


            loc.setLocation = function () {
                var center = window.map.getCenter();
                var zoom = window.map.getZoom();
                var factor = (128.0 / 180.0);
                loc.innerHTML = 
                    "zoom: " + zoom + 
                    "<br />x: " + (center.lng * factor) + 
                    "<br />y: " + (center.lat * factor);
            }

            loc.setLocation();

            window.map.addEventListener("move", loc.setLocation);


            return loc;
        },

        onRemove: function(map) {
            // Nothing to do here
        }
    });

    L.control.location = function(opts) {
        return new L.Control.Location(opts);
    }

    L.control.location({ position: 'topright' }).addTo(map);
    
    window.layer.addTo(window.map);
});
