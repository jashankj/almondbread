extern crate byteorder;
extern crate env_logger;
extern crate image;
extern crate log;
extern crate logger;
extern crate num;

pub mod color;
pub mod fractal;
