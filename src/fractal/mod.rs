use std::io::Result;

use image::{ImageBuffer, Rgb};
use color::Colorize;

pub mod burning_ship;
pub use self::burning_ship::BurningShip;

pub mod julia;
pub use self::julia::Julia;

pub mod mandelbrot;
pub use self::mandelbrot::Mandelbrot;

pub type RgbImageBuffer = ImageBuffer<Rgb<u8>,Vec<u8>>;

pub trait Fractal {
    fn paint<T>(&self, output: &mut RgbImageBuffer, size: u32) -> Result<()>
        where T: Colorize;
}
