use std::io::Result;

use image::{Luma, Rgb};
use num::complex::Complex;

use color::Colorize;
use fractal::{Fractal, RgbImageBuffer};

#[allow(non_camel_case_types)]
type c64 = Complex<f64>;

const MAX_ITER: usize = 256;

pub struct Mandelbrot {
    n: u8,
    cx: f64,
    cy: f64,
    scale: f64
}

impl Fractal for Mandelbrot {
    fn paint<T>(&self, output: &mut RgbImageBuffer, size: u32) -> Result<()>
    where T: Colorize {
        self.draw_mandelbrot_n(output, size, |px| { T::color_pixel(px) })
    }
}

impl Mandelbrot {
    pub fn new(n: u8, cx: f64, cy: f64, scale: f64) -> Mandelbrot {
        Mandelbrot {
            n: n,
            cx: cx,
            cy: cy,
            scale: scale
        }
    }

    pub fn draw_mandelbrot_n<F>(&self, output: &mut RgbImageBuffer, size: u32, color_fn: F) -> Result<()>
    where F: Fn(Luma<u8>) -> Rgb<u8> {
        for (px, py, pixel) in output.enumerate_pixels_mut() {
            let x = scale_point(px, self.cx, self.scale, size);
            let y = scale_point(size - py, self.cy, self.scale, size);
            let steps = escape_steps_n(self.n, x, y);
            // let steps =
            //     if n == 2 {
            //         assert_eq!(escape_steps(x, y), escape_steps_n(n, x, y));
            //         escape_steps(x, y)
            //     } else {
            //         escape_steps_n(n, x, y)
            //     };

            *pixel = color_fn(Luma([steps as u8]));
        }

        Ok(())
    }

}

fn scale_point(pixel_: u32, centre: f64, scale: f64, size_: u32) -> f64 {
    let pixel = pixel_ as f64;
    let size = size_ as f64;

    (((pixel - (size / 2.0)) / size) * scale) + centre
}

fn escape_steps_n(n: u8, x: f64, y: f64) -> usize {
    let mut iter = 1;

    let mu: c64 = c64 { re: x, im: y };

    let mut z: c64 = c64 { re: x, im: y };
    let mut z_ = z.pow_int(n);

    let bound = (2.0_f64).powf(n as f64);
    while z.norm_sqr() < bound && iter < MAX_ITER {
        z = z_ + mu;

        z_ = z.pow_int(n);
	iter += 1;
    }

    iter
}

use num::{Integer, Num};
trait IntegralPow<I>
where I: Integer {
    fn pow_int(&self, n: I) -> Self;
}

impl<I,T> IntegralPow<I> for Complex<T>
where I: Integer, T: Clone + Num {
    fn pow_int(&self, n_: I) -> Complex<T> {
        if n_ == I::zero() {
            return Complex::new(T::zero(), T::zero());
        } else if n_ == I::one() {
            return self.clone();
        }

        let mut n = n_;
        let mut ret = Complex::new(T::one(), T::zero());

        loop {
            ret = ret * self.clone();
            n = n - I::one();
            if n == I::zero() { break }
        }

        ret
    }
}

#[test] fn escape_steps_is_sane() {
    for x in (-20..20).map(|x| (x as f64) / 10.0) {
        for y in (-20..20).map(|x| (x as f64) / 10.0) {
            let es = escape_steps(x, y);
            let esn = escape_steps_n(2, x, y);
            println!("(x={},y={}) => {} {}", x, y, es, esn);
            // assert_eq! (es, esn);
        }
    }
}
