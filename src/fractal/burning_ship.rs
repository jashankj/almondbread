use std::io::Result;

use image::{Luma, Rgb};
use num::complex::Complex;

use color::Colorize;
use fractal::{Fractal, RgbImageBuffer};

#[allow(non_camel_case_types)]
type c64 = Complex<f64>;

const MAX_ITER: usize = 256;

pub struct BurningShip {
    cx: f64,
    cy: f64,
    scale: f64
}

impl Fractal for BurningShip {
    fn paint<T>(&self, output: &mut RgbImageBuffer, size: u32) -> Result<()>
    where T: Colorize {
        self.draw_burning_ship(output, size, |px| { T::color_pixel(px) })
    }
}

impl BurningShip {
    pub fn new(cx: f64, cy: f64, scale: f64) -> BurningShip {
        BurningShip {
            cx: cx,
            cy: cy,
            scale: scale
        }
    }

    pub fn draw_burning_ship<F>(&self, output: &mut RgbImageBuffer, size: u32, color_fn: F) -> Result<()>
    where F: Fn(Luma<u8>) -> Rgb<u8> {
        for (px, py, pixel) in output.enumerate_pixels_mut() {
            let x = scale_point(px, self.cx, self.scale, size);
            let y = scale_point(size - py, self.cy, self.scale, size);
            let steps = escape_steps(x, y);

            *pixel = color_fn(Luma([steps as u8]));
        }

        Ok(())
    }

}

fn scale_point(pixel_: u32, centre: f64, scale: f64, size_: u32) -> f64 {
    let pixel = pixel_ as f64;
    let size = size_ as f64;

    (((pixel - (size / 2.0)) / size) * scale) + centre
}

fn escape_steps(x: f64, y: f64) -> usize {
    let mut iter = 1;

    let mu: c64 = c64 { re: x, im: y };

    let mut z: c64 = c64 { re: 0.0, im: 0.0 };

    while z.norm_sqr() < 4.0 && iter < MAX_ITER {
        z = Complex::new(z.re.abs(), z.im.abs()).pow_int(2) + mu;
	iter += 1;
    }

    iter
}

use num::{Integer, Num};
trait IntegralPow<I>
where I: Integer {
    fn pow_int(&self, n: I) -> Self;
}

impl<I,T> IntegralPow<I> for Complex<T>
where I: Integer, T: Clone + Num {
    fn pow_int(&self, n_: I) -> Complex<T> {
        if n_ == I::zero() {
            return Complex::new(T::zero(), T::zero());
        } else if n_ == I::one() {
            return self.clone();
        }

        let mut n = n_;
        let mut ret = Complex::new(T::one(), T::zero());

        loop {
            ret = ret * self.clone();
            n = n - I::one();
            if n == I::zero() { break }
        }

        ret
    }
}
