use std::io::Result;

use image::{Luma, Rgb};
use num::complex::Complex;

use color::Colorize;
use fractal::{Fractal, RgbImageBuffer};

const MAX_ITER: usize = 256;

pub struct Julia {
    cx: f64,
    cy: f64,
    x: f64,
    y: f64,
    scale: f64
}

impl Fractal for Julia {
    fn paint<T: Colorize>(&self, output: &mut RgbImageBuffer, size: u32) -> Result<()> {
        self.draw_julia(output, size, |px| { T::color_pixel(px) })
    }
}

impl Julia {
    pub fn new(cx: f64, cy: f64, x: f64, y: f64, scale: f64) -> Julia {
        Julia {
            cx: cx,
            cy: cy,
            x: x,
            y: y,
            scale: scale
        }
    }

    pub fn draw_julia<F>(&self, output: &mut RgbImageBuffer, size: u32, color_fn: F) -> Result<()>
    where F: Fn(Luma<u8>) -> Rgb<u8> {
        for (px, py, pixel) in output.enumerate_pixels_mut() {
            let x = scale_point(px, self.x, self.scale, size);
            let y = scale_point(size - py, self.y, self.scale, size);

            let mut z = Complex::new(x, y);
            let c = Complex::new(self.cx, self.cy);

            let mut i = 0;

            for t in 0..MAX_ITER {
                if z.norm() > 2.0 {
                    break
                }
                z = z * z + c;
                i = t;
            }

            // Create an 8bit pixel of type Luma and value i
            // and assign in to the pixel at position (x, y)
            *pixel = color_fn(Luma([i as u8]));
        }

        Ok(())
    }
}

fn scale_point(pixel_: u32, centre: f64, scale: f64, size_: u32) -> f64 {
    let pixel = pixel_ as f64;
    let size = size_ as f64;

    (((pixel - (size / 2.0)) / size) * scale) + centre
}
