use image::{Luma, Rgb};
use color::Colorize;

pub struct Orange;
impl Colorize for Orange {
    fn color_pixel(pixel: Luma<u8>) -> Rgb<u8> {
        Rgb([
            pixel.data[0],
            ((96.0 * (pixel.data[0] as f32)) / 256.0) as u8,
            0
        ])
    }
}
