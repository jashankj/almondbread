use image::{Luma, Rgb};

pub mod lumiance;
pub use self::lumiance::Lumiance;

pub mod orange;
pub use self::orange::Orange;

pub mod xaos;
pub use self::xaos::Xaos;

pub trait Colorize {
    fn color_pixel(pixel: Luma<u8>) -> Rgb<u8>;
}
