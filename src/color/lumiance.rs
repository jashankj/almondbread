use image::{Luma, Rgb};
use color::Colorize;

pub struct Lumiance;
impl Colorize for Lumiance {
    fn color_pixel(pixel: Luma<u8>) -> Rgb<u8> {
        Rgb([
            pixel.data[0],
            pixel.data[0],
            pixel.data[0]
        ])
    }
}
