extern crate almondbread;
extern crate byteorder;
extern crate env_logger;
extern crate image;
#[macro_use] extern crate iron;
extern crate log;
extern crate logger;
#[macro_use] extern crate mime;
extern crate num;
extern crate num_cpus;
#[macro_use] extern crate router;

use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

use image::{PNG, ImageBuffer, ImageRgb8};
use iron::headers::{ContentType, CacheControl, CacheDirective};
use iron::prelude::*;
use iron::status;
use logger::Logger;
use router::Router;

use almondbread::fractal::{self, Fractal, RgbImageBuffer};
use almondbread::color;

macro_rules! router_arg {
    ($req:expr, $e:expr, $t:ty) => {{
        let ref arg_str =
            iexpect!(iexpect!($req.extensions.get::<Router>()).find($e));
        itry!((*arg_str).parse::<$t>())
    }}
}

macro_rules! fractal {
    ( $t:ident , $( $i:ident : $it:ty ),* ) => {|req: &mut Request| {
        let size = 512;

        $( let $i = router_arg!(req, stringify!($i), $it); )*
        let z = router_arg!(req, "z", f64);
        let scale = (2.0_f64).powf(9.0 - z);

        let mut imgbuf: RgbImageBuffer = ImageBuffer::new(size, size);
        fractal::$t::new($( $i ),* , scale)
            .paint::<color::Xaos>(&mut imgbuf, size)
            .unwrap();

        let mut ret: Vec<u8> = Vec::new();
        itry!(ImageRgb8(imgbuf).save(&mut ret, PNG));

        let mut res = Response::with((status::Ok, ret));
        res.headers.set(ContentType(mime!(Image/Png)));
        let age: u32 = (24 * 60 * 60) * 7; // seven days, in seconds
        res.headers.set(CacheControl(vec![CacheDirective::MaxAge(age)]));
        Ok(res)
    }}
}

fn main() {
    env_logger::init().unwrap();

    let router = router!(
        index: get "/" => render_viewer,
        tilesjs: get "/tiles.js" => render_tiles_js,

        mandelbrot: get "/mandelbrot/:n/:z/:x/:y/tile.bmp" =>
            fractal!(Mandelbrot, n: u8, x: f64, y: f64),

        julia: get "/julia/:cx/:cy/:z/:x/:y/tile.bmp" =>
            fractal!(Julia, cx: f64, cy: f64, x: f64, y: f64),

        burning_ship: get "/burning-ship/:z/:x/:y/tile.bmp" =>
            fractal!(BurningShip, x: f64, y: f64),
    );

    let (logger_before, logger_after) = Logger::new(None);
    let mut chain = Chain::new(router);

    chain.link_before(logger_before);
    chain.link_after(logger_after);

    let mut server = Iron::new(chain);
    server.threads = ::num_cpus::get();
    server.http("localhost:3000").unwrap();
}

fn render_viewer(_: &mut Request) -> Result<Response, IronError> {
    let viewer = r#"
<script src="/tiles.js"></script>
"#;

    let mut res = Response::with((status::Ok, viewer));
    res.headers.set(ContentType(mime!(Text/Html)));
    Ok(res)
}

fn render_tiles_js(_: &mut Request) -> Result<Response, IronError> {
    // horrible static file hack
    let file = itry!(File::open("tiles.js"));
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    itry!(buf_reader.read_to_string(&mut contents));

    // let contents = include_str!("../tiles.js");

    let mut res = Response::with((status::Ok, contents));
    res.headers.set(ContentType(mime!(Text/Javascript)));
    Ok(res)
}
